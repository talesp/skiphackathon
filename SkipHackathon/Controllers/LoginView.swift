//
// Created by Tales Pinheiro De Andrade on 17/03/18.
// Copyright (c) 2018 Tales Pinheiro De Andrade. All rights reserved.
//

import UIKit

class LoginView: UIView {
    lazy private(set) var emailLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    lazy private(set) var passwordLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    lazy private(set) var emailField: UITextField = {
        let field = UITextField(frame: .zero)
        field.translatesAutoresizingMaskIntoConstraints = false
        return field
    }()

    lazy private(set) var passwordField: UITextField = {
        let field = UITextField(frame: .zero)
        field.translatesAutoresizingMaskIntoConstraints = false
        return field
    }()

    lazy private(set) var signinButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    lazy private(set) var signupButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViewConfiguration()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension LoginView: ViewConfiguration {
    func buildViewHierarchy() {
        addSubview(emailLabel)
        addSubview(emailField)
        addSubview(passwordLabel)
        addSubview(passwordField)
        addSubview(signupButton)
        addSubview(signinButton)
    }

    func setupConstraints() {
        NSLayoutConstraint.activate([
            emailLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            emailField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            passwordLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            passwordField.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            signupButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            signinButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),

            emailLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 8),
            emailField.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 8),
            passwordLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 8),
            passwordField.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 8),
            signupButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 8),
            signinButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 8),

            emailLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 16),
            emailLabel.bottomAnchor.constraint(equalTo: emailField.topAnchor, constant: -8),
            emailField.bottomAnchor.constraint(equalTo: passwordLabel.topAnchor, constant: -16),
            passwordLabel.bottomAnchor.constraint(equalTo: passwordField.topAnchor, constant: -8),
            passwordField.bottomAnchor.constraint(equalTo: signinButton.topAnchor, constant: -8),
            signinButton.bottomAnchor.constraint(equalTo: signupButton.topAnchor, constant: -8),
            signupButton.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16),

            emailLabel.heightAnchor.constraint(equalTo: passwordLabel.heightAnchor),
            emailField.heightAnchor.constraint(equalTo: passwordField.heightAnchor),

            signinButton.heightAnchor.constraint(equalToConstant: 44.0),
            signupButton.heightAnchor.constraint(equalToConstant: 44.0)
            ])

    }

    func configureViews() {
        self.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        self.emailLabel.text = "Email"
        self.emailField.placeholder = "Email"
        self.emailField.autocorrectionType = UITextAutocorrectionType.no
        self.emailField.autocapitalizationType = .none
        self.passwordLabel.text = "Password"
        self.passwordField.placeholder = "Password"
        self.passwordField.autocorrectionType = .no
        self.passwordField.autocapitalizationType = .none
        self.passwordField.isSecureTextEntry = true

        self.signinButton.setTitle("Signin", for: .normal)
        self.signupButton.setTitle("Signup", for: .normal)
        emailLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)
        emailField.setContentHuggingPriority(.defaultHigh, for: .vertical)
        passwordLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)
        passwordField.setContentHuggingPriority(.defaultHigh, for: .vertical)
    }
}
