//
//  StoreListDelegateDataSource.swift
//  SkipHackathon
//
//  Created by Tales Pinheiro De Andrade on 17/03/18.
//  Copyright © 2018 Tales Pinheiro De Andrade. All rights reserved.
//

import UIKit

class StoreListDelegateDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {

    var stores: [Store] = []
    private static let cellIdentifier = "storeCell"
    var storeDetail: (Store) -> Void

    init(storeDetail: @escaping (Store) -> Void) {
        self.storeDetail = storeDetail
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stores.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let store = stores[indexPath.row]

        if let cell = tableView.dequeueReusableCell(withIdentifier: StoreListDelegateDataSource.cellIdentifier) {
            cell.textLabel?.text = store.name
            cell.detailTextLabel?.text = store.address
            cell.detailTextLabel?.numberOfLines = 0
            return cell
        } else {
            let cell = UITableViewCell(style: UITableViewCellStyle.subtitle,
                                       reuseIdentifier: StoreListDelegateDataSource.cellIdentifier)
            cell.textLabel?.text = store.name
            cell.detailTextLabel?.text = store.address
            cell.detailTextLabel?.numberOfLines = 0
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let store = stores[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        self.storeDetail(store)
    }
}
