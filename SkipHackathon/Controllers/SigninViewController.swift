//
//  SigninViewController.swift
//  SkipHackathon
//
//  Created by Tales Pinheiro De Andrade on 17/03/18.
//  Copyright © 2018 Tales Pinheiro De Andrade. All rights reserved.
//

import UIKit

class SigninViewController: UIViewController {

    lazy private var loginView: LoginView = {
        let view = LoginView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewConfiguration()
    }

    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @objc
    func signin(sender: UIButton) {
        guard let email = loginView.emailField.text, email.isEmpty == false,
            let password = loginView.passwordField.text, password.isEmpty == false else {
                let alertAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                let alertController = UIAlertController(title: nil,
                                                        message: "You need to provide email and password",
                                                        preferredStyle: .alert)
                alertController.addAction(alertAction)
                self.present(alertController, animated: true, completion: nil)
                return
        }
        Webservice().load(CustomerAuthentication().authenticate(email: email, password: password)) { result in
            //TODO: implement
            switch result {
            case .success(let customerAuth):
                Settings.shared.customerAuthentication = customerAuth
                DispatchQueue.main.async {
                    let tabbbarController = UITabBarController()
                    tabbbarController.viewControllers = [
                        UINavigationController(rootViewController: StoreListViewController()) ,
                        UINavigationController(rootViewController: OrderListViewController())
                    ]
                    tabbbarController.modalPresentationStyle = .fullScreen
                    self.present(tabbbarController, animated: true, completion: nil)
                }
            case .error(let error):
                print(error)
            }
        }
    }

    @objc
    func signup(sender: UIButton) {

    }
}

extension SigninViewController: ViewConfiguration {
    func buildViewHierarchy() {
        self.view.addSubview(loginView)
    }

    func setupConstraints() {
        NSLayoutConstraint.activate([
        loginView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
        loginView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
        loginView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 50)
//        loginView.bottomAnchor.constraint(greaterThanOrEqualTo: self.view.safeAreaLayoutGuide.bottomAnchor,
//                                          constant: -100)
        ])
    }

    func configureViews() {
        self.loginView.signinButton.addTarget(self, action: #selector(signin(sender:)), for: .touchUpInside)
        self.loginView.signupButton.addTarget(self, action: #selector(signup(sender:)), for: .touchUpInside)
        self.view.backgroundColor = .red
    }


}
