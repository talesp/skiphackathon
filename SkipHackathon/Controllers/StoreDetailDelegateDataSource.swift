//
//  StoreDetailDelegateDataSource.swift
//  SkipHackathon
//
//  Created by Tales Pinheiro De Andrade on 17/03/18.
//  Copyright © 2018 Tales Pinheiro De Andrade. All rights reserved.
//

import UIKit

class StoreDetailDelegateDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    var products: [Product] = []
    private static let cellIdentifier = "productCell"

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let product = products[indexPath.row]

        if let cell = tableView.dequeueReusableCell(withIdentifier: StoreDetailDelegateDataSource.cellIdentifier) {
            cell.textLabel?.text = product.name
            cell.detailTextLabel?.text = product.description
            return cell
        } else {
            let cell = UITableViewCell(style: UITableViewCellStyle.subtitle,
                                       reuseIdentifier: StoreDetailDelegateDataSource.cellIdentifier)
            cell.textLabel?.text = product.name
            cell.detailTextLabel?.text = product.description
            return cell
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
