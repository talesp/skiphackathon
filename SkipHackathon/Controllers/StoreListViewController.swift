//
//  StoreListViewController.swift
//  SkipHackathon
//
//  Created by Tales Pinheiro De Andrade on 17/03/18.
//  Copyright © 2018 Tales Pinheiro De Andrade. All rights reserved.
//

import UIKit

class StoreListViewController: UIViewController {

    private var delegateDataSource: (UITableViewDelegate & UITableViewDataSource)?

    lazy private(set) var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Stores"

        Webservice().load(Store.all) { [weak self] result in
            switch result {
            case .success(let stores):
                guard let dataSource = self?.delegateDataSource as? StoreListDelegateDataSource else { fatalError() }
                dataSource.stores = stores
                DispatchQueue.main.async {
                    print(dataSource)
                    self?.tableView.reloadData()
                }
            case .error(let error):
                fatalError("Error: [\(error)]")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    init() {
        super.init(nibName: nil, bundle: nil)
        setupViewConfiguration()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension StoreListViewController: ViewConfiguration {
    func buildViewHierarchy() {
        self.view.addSubview(self.tableView)
    }

    func setupConstraints() {
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
            ])

    }

    func configureViews() {
        let delegateDataSource = StoreListDelegateDataSource { store in
            let storeDetailsViewContorller = StoreDetailViewController(store: store)
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(storeDetailsViewContorller, animated: true)
            }
        }
        tableView.delegate = delegateDataSource
        tableView.dataSource = delegateDataSource
        self.delegateDataSource = delegateDataSource

    }

}
