//
//  OrderItem.swift
//  SkipHackathon
//
//  Created by Tales Pinheiro De Andrade on 17/03/18.
//  Copyright © 2018 Tales Pinheiro De Andrade. All rights reserved.
//

import Foundation

struct OrderItem: Codable {
    let identifier: Int
    let orderId: Int
    let productId: Int
    let product: Product
    let price: Double
    let quantity: Int
    let total: Double
}
