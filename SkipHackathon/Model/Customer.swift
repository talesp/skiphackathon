//
//  Customer.swift
//  SkipHackathon
//
//  Created by Tales Pinheiro De Andrade on 17/03/18.
//  Copyright © 2018 Tales Pinheiro De Andrade. All rights reserved.
//

import Foundation

struct Customer: Codable {
    let identifier: Int
    let email: String
    let name: String
    let address: String
    let creation: Date
    let password: String
    let authentication: CustomerAuthentication? = nil
}
