//
//  Order.swift
//  SkipHackathon
//
//  Created by Tales Pinheiro De Andrade on 17/03/18.
//  Copyright © 2018 Tales Pinheiro De Andrade. All rights reserved.
//

import Foundation

struct Order: Codable {
    let identifier: Int
    let date: Date
    let customerID: Int
    let deliveryAddress: String
    let contact: String
    let storeId: Int
    let orderItems: [OrderItem]
    let total: Double
    let status: String
    let lastUpdate: Date
}
