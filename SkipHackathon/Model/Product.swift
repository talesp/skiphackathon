//
//  Product.swift
//  SkipHackathon
//
//  Created by Tales Pinheiro De Andrade on 17/03/18.
//  Copyright © 2018 Tales Pinheiro De Andrade. All rights reserved.
//

import Foundation

struct Product: Codable {
    let identifier: Int
    let storeId: Int
    let name: String
    let description: String
    let price: Double

    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case storeId
        case name
        case description
        case price
    }
}
