//
//  Store.swift
//  SkipHackathon
//
//  Created by Tales Pinheiro De Andrade on 17/03/18.
//  Copyright © 2018 Tales Pinheiro De Andrade. All rights reserved.
//

import Foundation

struct Store: Codable {
    let identifier: Int
    let name: String
    let address: String
    let cousineId: Int

}
