//
//  Cousine+Resource.swift
//  SkipHackathon
//
//  Created by Tales Pinheiro De Andrade on 17/03/18.
//  Copyright © 2018 Tales Pinheiro De Andrade. All rights reserved.
//

import Foundation

extension Cousine {

    private static let pathURLString = "/Customer"
    private(set) static var baseURL: URL = Settings.shared.baseURL

    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case name
    }

    func load() -> Resource<Cousine> {
        let url = Cousine.baseURL.appendingPathComponent(Cousine.pathURLString)
        return Resource(url: url)
    }
}
