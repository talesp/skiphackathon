//
//  Customer+Resource.swift
//  SkipHackathon
//
//  Created by Tales Pinheiro De Andrade on 17/03/18.
//  Copyright © 2018 Tales Pinheiro De Andrade. All rights reserved.
//

import Foundation

extension Customer {
    private static let pathURLString = "/Customer"
    private(set) static var baseURL: URL = Settings.shared.baseURL

    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case email
        case name
        case address
        case creation
        case password
    }

    func create(identifier: Int = 0,
                email: String,
                name: String,
                address: String,
                password: String
                ) -> Resource<Customer> {
        let url = Customer.baseURL.appendingPathComponent(Customer.pathURLString)

        return Resource(url: url)
    }
}
