//
//  Store+Resource.swift
//  SkipHackathon
//
//  Created by Tales Pinheiro De Andrade on 17/03/18.
//  Copyright © 2018 Tales Pinheiro De Andrade. All rights reserved.
//

import Foundation

extension Store {

    private static let pathURLString = "/Store"
    private(set) static var baseURL: URL = Settings.shared.baseURL

    enum CodingKeys: String, CodingKey {
        case identifier = "id"
        case name
        case address
        case cousineId
    }

    static let all: Resource<[Store]> = {
        let url = Store.baseURL.appendingPathComponent(Store.pathURLString)
        return Resource(url: url)
    }()

    func products() -> Resource<[Product]> {
        let pathString = "\(Store.pathURLString)/\(self.identifier)/products"
        let url = Store.baseURL.appendingPathComponent(pathString)
        return Resource(url: url)
    }
}
