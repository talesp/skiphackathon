//
//  CustomerAuthentication+Resource.swift
//  SkipHackathon
//
//  Created by Tales Pinheiro De Andrade on 17/03/18.
//  Copyright © 2018 Tales Pinheiro De Andrade. All rights reserved.
//

import Foundation

extension CustomerAuthentication {
    static let pathURLString = "/Customer/auth"
    private(set) static var baseURL: URL = Settings.shared.baseURL

    func authenticate(email: String, password: String) -> Resource<CustomerAuthentication> {

        let url = Customer.baseURL.appendingPathComponent(CustomerAuthentication.pathURLString)

        var components = URLComponents(url: url, resolvingAgainstBaseURL: false)
        components?.queryItems = [
            URLQueryItem(name: "email", value: email),
            URLQueryItem(name: "password", value: password)
        ]
        guard let resourceURL = components?.url else {
            fatalError("Error creating authentication URL")
        }
        return Resource(url: resourceURL, method: .post([:]))
    }

}
