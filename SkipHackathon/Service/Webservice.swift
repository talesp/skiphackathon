//
//  Webservice.swift
//  SkipHackathon
//
//  Created by Tales Pinheiro De Andrade on 17/03/18.
//  Copyright © 2018 Tales Pinheiro De Andrade. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(T)
    case error(Error)

    init(_ value: T) {
        self = Result.success(value)
    }

    init(_ value: Error) {
        self = Result.error(value)
    }
}

final class Webservice {

    let urlSession: URLSession

    init(urlSession: URLSession = URLSession.shared) {
        self.urlSession = urlSession
    }

    func load<T>(_ resource: Resource<T>, decoder: JSONDecoder = JSONDecoder(), completion: @escaping (Result<T>) -> Void) {
        let request = URLRequest(resource: resource)
        let task = urlSession.dataTask(with: request) { data, urlResponse, error in
            let result: Result<T>!
            if let response = urlResponse as? HTTPURLResponse {
                switch response.statusCode {
                case 200..<299:
                    if let data = data {
                        do {
                            if resource is Resource<CustomerAuthentication> {
                                guard let token = String(data: data, encoding: .utf8) else { fatalError() }
                                let customerAuthentication = CustomerAuthentication(token: token)
                                result = Result(customerAuthentication as! T)
                            } else {
                                result = try Result(resource.parse(data))
                            }
                        }
                        catch let error {
                            result = Result(error)
                        }
                    }
                    else if let error = error {
                        result = Result(error)
                    }
                    else {
                        fatalError("FIXME")
                    }
                case 300..<399:
                    fatalError("FIXME")
                case 400..<499:
                    result = Result(error!)
                default:
                    fatalError("FIXME")
                }
            }
            else if let data = data {
                fatalError("FIXME: [\(data)]")
            }
            else if let error = error {
                result = Result(error)
            } else {
                fatalError()
            }
            completion(result)
        }
        task.resume()
    }
}

