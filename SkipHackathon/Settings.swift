//
//  Settings.swift
//  SkipHackathon
//
//  Created by Tales Pinheiro De Andrade on 17/03/18.
//  Copyright © 2018 Tales Pinheiro De Andrade. All rights reserved.
//

import Foundation

final class Settings {
    let baseURL: URL
    var customerAuthentication: CustomerAuthentication?

    init(baseURLString: String = "http://api-vanhack-event-sp.azurewebsites.net/api/v1") {
        guard let baseURL = URL(string: baseURLString) else {
            fatalError("Error parsing URLString: [\(baseURLString)]")
        }
        self.baseURL = baseURL
        self.customerAuthentication = nil
    }

    static let shared: Settings = Settings()
}
